<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>userDelete</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>
	<header style="background-color: #484b48;">
		<div class="row" style="color: #fcf9f8;">
			<div class="col-8"></div>
			<div class="col-2">
				<span> ${userInfo.name} さん </span> 
			</div>
			<div class="col-2">
				<a href="LogoutServlet">ログアウト</a>
			</div>
		</div>
	</header>

	<h1 class="row justify-content-md-center">ユーザー削除確認</h1>
	<div class="row">
		<p></p>
	</div>

	<div class="row">
		<div class="col-2"></div>
		<div class="col-10">ログインID：${userData.loginId}</div>
		
	</div>
	<div class="row">
		<div class="col-2"></div>
		<div class="col-10">を本当に削除してよろしいでしょうか。</div>
	</div>
	
	<div>
		<p></p>
	</div>

	<div class="row">
		<div class="col-2"></div>	
	
		<div class="col-4">
			<form action="UserListServlet" method="get">		
				<button type="submit" class="btn btn-secondary">キャンセル</button>
			</form>
		</div>
		
		<div class="col-4">
			<form action="UserDeleteServlet" method="POST">
				<button type="submit" class="btn btn-danger">OK</button>
				<input type="hidden" readonly class="form-control-plaintext" id="loginId" 
				name = "loginId" value="${userData.loginId}">
			</form>
		</div>
		<div class="col-2"></div>
		
	</div>
	

</body>
</html>