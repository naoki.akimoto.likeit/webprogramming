<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>userRegister</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>
	<header style="background-color: #484b48;">
		<div class="row" style="color: #fcf9f8;">
			<div class="col-8"></div>
			<div class="col-2">
				<span> ${userInfo.name} さん </span>
			</div>
			<div class="col-2">
				<a href="LogoutServlet">ログアウト</a>
			</div>
		</div>
	</header>

	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

	<div class="row">
		<p></p>
	</div>

	<h1 class="row justify-content-md-center">ユーザー新規登録</h1>
	<form action="UserRegisterServlet" method="post">

		<div class="row">
			<p></p>
		</div>

		<div class="row">
			<div class="col-4">
				<label for="exampleInputPassword1">ログインID</label>
			</div>
			<div class="col-4">
				<input type="text" class="form-control" id="userID" name="loginId"
					placeholder="ログインID">
			</div>
		</div>

		<div class="row">
			<p></p>
		</div>

		<div class="row">
			<div class="col-4">
				<label for="exampleInputPassword1">パスワード</label>
			</div>
			<div class="col-4">
				<input type="password" class="form-control" id="userPassword" name="password"
					placeholder="パスワード">
			</div>
		</div>

		<div class="row">
			<p></p>
		</div>

		<div class="row">
			<div class="col-4">
				<label for="exampleInputPassword1">パスワード(確認用)</label>
			</div>
			<div class="col-4">
				<input type="password" class="form-control" id="checkPassword" name="checkPassword"
					placeholder="パスワード(確認用)">
			</div>
		</div>


		<div class="row">
			<p></p>
		</div>

		<div class="row">
			<div class="col-4">
				<label for="exampleInputPassword1">ユーザー名</label>
			</div>
			<div class="col-4">
				<input type="text" class="form-control" id="userName" name="name"
					placeholder="ユーザー名">
			</div>
		</div>

		<div class="row">
			<p></p>
		</div>

		<div class="row">
			<div class="col-4">
				<label for="exampleInputPassword1">生年月日</label>
			</div>
			<div class="col-4">
				<input type="text" class="form-control" id="userBirthday" name="birthDate"
					placeholder="20XX-0X-0X (半角英数字)">
			</div>
		</div>
		
	<div class="row">
		<p></p>
	</div>

	<div align="center">
		<button type="submit" class="btn btn-primary">登録</button>
	</div>
	</form>

	<div align="left">
		<a href="UserListServlet">戻る</a>
	</div>


</body>

</html>