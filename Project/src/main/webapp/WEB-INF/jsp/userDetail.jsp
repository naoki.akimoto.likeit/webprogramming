<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>userDetail</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>
	<header style="background-color: #484b48;">
		<div class="row" style="color: #fcf9f8;">
			<div class="col-8"></div>
			<div class="col-2">
				<span> ${userInfo.name} さん </span> 
			</div>
			<div class="col-2">
				<a href="LogoutServlet">ログアウト</a>
			</div>
		</div>
	</header>

	<div class="row">
		<p></p>
	</div>

	<h1 class="row justify-content-md-center">ユーザー情報詳細参照</h1>

		<div class="row">
			<p></p>
		</div>

		<div class="row">
			<div class="col-4">ログインID</div>
			<div class="col-8">${userDetail.loginId} </div>
		</div>

		<div class="row">
			<p></p>
		</div>

		<div class="row">
			<div class="col-4">ユーザー名</div>
			<div class="col-8">${userDetail.name}</div>
		</div>

		<div class="row">
			<p></p>
		</div>

		<div class="row">
			<div class="col-4">生年月日</div>
			<div class="col-8">${userDetail.birthDate}</div>
		</div>

		<div class="row">
			<p></p>
		</div>

		<div class="row">
			<div class="col-4">登録日時</div>
			<div class="col-8">${userDetail.createDate}</div>
		</div>

		<div class="row">
			<p></p>
		</div>

		<div class="row">
			<div class="col-4">更新日時</div>
			<div class="col-8">${userDetail.updateDate}</div>
		</div>

		<div class="row">
			<p></p>
		</div>

		<div align="left">
			<a href="UserListServlet">戻る</a>
		</div>


</body>
</html>