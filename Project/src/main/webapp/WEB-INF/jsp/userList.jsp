<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>userList</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>
	<header style="background-color: #484b48;">
		<div class="row" style="color: #fcf9f8;">
			<div class="col-8"></div>
			<div class="col-2">
				<span> ${userInfo.name} さん </span>
			</div>
			<div class="col-2">
				<a href="LogoutServlet">ログアウト</a>
			</div>
		</div>
	</header>

	<h1 class="row justify-content-md-center">ユーザ一覧</h1>
	<div align="right">
		<a href="UserRegisterServlet">新規登録</a>
	</div>
	<form action="UserListServlet" method="post">
		<div class="row">
			<div class="col-4">
				<label for="exampleInputPassword1">ログインID</label>
			</div>
			<div class="col-4">
				<input type="text" class="form-control" id="userId" name="userId"
					placeholder="ログインID">
			</div>
		</div>

		<div class="row">
			<p></p>
		</div>

		<div class="row">
			<div class="col-4">
				<label for="exampleInputPassword1">ユーザー名</label>
			</div>
			<div class="col-4">
				<input type="text" class="form-control" id="userName" name="userName"
					placeholder="ユーザー名">
			</div>
		</div>

		<div class="row">
			<p></p>
		</div>

		<div class="row">
			<div class="col-4">
				<label for="exampleInputPassword1">生年月日</label>
			</div>
			<div class="col-3">
				<input type="text" class="form-control" id="birthday1" name="birthdate1"
					placeholder="20XX-0X-0X (半角英数字)">
			</div>
			<div>〜</div>

			<div class="col-3">
				<input type="text" class="form-control" id="birthday2" name="birthdate2"
					placeholder="20XX-0X-0X (半角英数字)">
			</div>
		</div>

		<div class="row">
			<p></p>
		</div>

		<div align="right">
			<button type="submit" class="btn btn-primary">検索</button>
		</div>

	</form>

	<hr>

	<div class="table-responsive">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>ログインID</th>
					<th>ユーザ名</th>
					<th>生年月日</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${userList}">
					<tr>
						<td>${user.loginId}</td>
						<td>${user.name}</td>
						<td>${user.birthDate}</td>
						<!-- TODO 未実装；ログインボタンの表示制御を行う -->
						<td>
						<c:choose>
							<c:when test="${userInfo.loginId == 'admin'}">
								<a class="btn btn-primary"href="UserDetailServlet?id=${user.id}">詳細</a> 
 								<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
								<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a>
							</c:when>
							<c:when test="${userInfo.loginId == user.loginId}">
								<a class="btn btn-primary"href="UserDetailServlet?id=${user.id}">詳細</a> 
 								<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
								<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a>
							</c:when>
							<c:otherwise>
								<a class="btn btn-primary"href="UserDetailServlet?id=${user.id}">詳細</a> 								
							</c:otherwise>
 						
						</c:choose>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</body>

</html>