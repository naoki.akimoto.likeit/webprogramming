<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<link rel="stylesheet"
		href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
		crossorigin="anonymous">
</head>

<body>
	<header style="background-color: #484b48;">
		<div class="row" style="color: #fcf9f8;"></div>
	</header>
	
	<div>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

	<div class="row">
		<p></p>
	</div>

	<h1 class="row justify-content-md-center">ログイン画面</h1>

	<div class="row">
		<p></p>
	</div>

	<form action="LoginServlet" method="post">
		<div class="row justify-content-md-center">
			<div class="form-group">
				<label for="exampleInputPassword1">ログインID</label> 
				<input type="text" class="form-control" name="loginId" id="loginId" placeholder="ログインID">
			</div>
		</div>

		<div class="row justify-content-md-center">
			<div class="form-group">
				<label for="exampleInputPassword1">パスワード</label> 
				<input type="password" class="form-control" name="password" id="password" placeholder="パスワード">
			</div>
		</div>

		<div class="row justify-content-md-center">
			<button type="submit" class="btn btn-primary">ログイン</button>
		</div>
	</form>
	</div>
	
</body>
</html>

