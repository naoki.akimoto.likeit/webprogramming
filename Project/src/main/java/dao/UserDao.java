package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

    /**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     * @param loginId
     * @param password
     * @return
     */
    public User findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, password);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


    /**
     * 全てのユーザ情報を取得する
     * @return
     */
    public List<User> findAll() {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id != ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, "admin");
            ResultSet rs = pStmt.executeQuery();

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("creat_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }
    
    //検索処理
    public List<User> findPart(String userId, String userName, String birthdate1, String birthdate2) {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();
            int num = 1;
            int managementNum =1;
            int userIdNum = 0;
            int userNameNum = 0;
            int userBirthdate1Num = 0;
            int userBirthdate2Num = 0;
 
            // SELECT文を準備
            StringBuilder sql = new StringBuilder("SELECT * FROM user WHERE login_id != ? ");
            
            if(!(userId.equals(""))) {
                sql.append(" AND login_id = ?");
                num ++;
                userIdNum = num;
            }
            if(!(userName.equals(""))) {
                sql.append(" AND name LIKE ?");
                num ++;
                userNameNum = num;
                
            }
            if(!(birthdate1.equals(""))) {
                sql.append(" AND birth_date >= ?");
                num ++;
                userBirthdate1Num = num;
            }
            if(!(birthdate2.equals(""))) {
                sql.append(" AND birth_date <= ?");
                num ++;
                userBirthdate2Num = num;
            }
            
//          String sql = "SELECT * FROM user WHERE login_id != ? AND login_id = ? AND name LIKE ? AND birth_date >= ? AND birth_date <= ?";
            System.out.println(sql.toString());

            // SELECTを実行し、結果表を取得
           PreparedStatement pStmt = conn.prepareStatement(sql.toString());
           pStmt.setString(managementNum, "admin");
           
           if(!(userId.equals(""))) {
        	   pStmt.setString(userIdNum, userId);
           }
           if(!(userName.equals(""))) {
        	   pStmt.setString(userNameNum, "%"+ userName +"%");
           }
           if(!(birthdate1.equals(""))) {
        	   pStmt.setString(userBirthdate1Num, birthdate1);
           }
           if(!(birthdate2.equals(""))) {
        	   pStmt.setString(userBirthdate2Num, birthdate2);
           }
           
           ResultSet rs = pStmt.executeQuery();

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("creat_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }
    
    
    public User confirmUserId(String loginId) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            ResultSet rs = pStmt.executeQuery();

            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }
            int id = rs.getInt("id");

            return new User(id);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

	public void insertUser(String loginId, String password, String name, String birthDate) {
	    Connection conn = null;
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "INSERT INTO user (login_id, name, birth_date, password,creat_date, update_date) VALUES ( ?, ?, ?, ?, now(), now())";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, loginId);
	        pStmt.setString(2, name);
	        pStmt.setString(3, birthDate);
	        pStmt.setString(4, password);
	        
	        pStmt.executeUpdate();       
	        
	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}
	
	
	
	public User findUserData(String id) {
        Connection conn = null;
        int Id = Integer.parseInt(id);
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setInt(1, Id);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            int idDate = rs.getInt("id");
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            Date birthDateData = rs.getDate("birth_date");
            String passwordData = rs.getString("password");
            String createDateData = rs.getString("creat_date");
            String updateDateData = rs.getString("update_date");

            return new User(idDate, loginIdData, nameData, birthDateData, passwordData, createDateData, updateDateData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
	
	public void updateUserData(String name, String birthDate, String loginId) {
	    Connection conn = null;
        
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "UPDATE user SET name = ?, birth_date = ?, update_date = now() WHERE login_id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, name);
	        pStmt.setString(2, birthDate);
	        pStmt.setString(3, loginId);
        
	        pStmt.executeUpdate();
	       	
	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}
	
	public void updateUserData(String password, String name, String birthDate, String loginId) {
	    Connection conn = null;
        
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "UPDATE user SET password = ?, name = ?, birth_date = ?, update_date = now() WHERE login_id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, password);
	        pStmt.setString(2, name);
	        pStmt.setString(3, birthDate);
	        pStmt.setString(4, loginId);
        
	        pStmt.executeUpdate();
	       	
	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}
	
	public void deleteUserData(String loginId) {
	    Connection conn = null;

	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();	        	        
	
	        // SELECT文を準備
	        String sql = "DELETE FROM user WHERE login_id = ?";
	
	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, loginId);
	      
	        pStmt.executeUpdate();

	    } catch (SQLException e) {
	        e.printStackTrace();
	        
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                
	            }
	        }
	    }
	}
}
